//
//  MovieCollectionViewCell.swift
//  desafio_sky
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var uiThumb: UIImageView?
    @IBOutlet weak var uiName: UILabel?
    @IBOutlet weak var uiAge: UILabel?
    
}
