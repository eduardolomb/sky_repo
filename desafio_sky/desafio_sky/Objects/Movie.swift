//
//  Movie.swift
//  desafio_sky
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import Foundation

class Movie {

    let name:String?
    let thumb:String?
    
    init(name:String?, thumb:String?) {
        self.name = name
        self.thumb = thumb
    }
    
}
