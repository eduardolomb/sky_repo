//
//  MovieCollectionReusableView.swift
//  desafio_sky
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit

class MovieCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var uiHeaderLabel: UILabel?
}
