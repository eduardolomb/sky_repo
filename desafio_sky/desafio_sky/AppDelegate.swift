//
//  AppDelegate.swift
//  desafio_sky
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        UIApplication.shared.statusBarStyle = .lightContent

        return true
    }

   

}

