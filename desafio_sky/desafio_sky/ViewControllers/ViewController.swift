//
//  ViewController.swift
//  desafio_sky
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController:
UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var uiMovieCollection: UICollectionView?
    var moviesItens:NSArray = []
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return moviesItens.count
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let h = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "movieHeader", for: indexPath) as? MovieCollectionReusableView
        
        h?.uiHeaderLabel?.text = "Uma seleção de filmes imperdíveis"
        guard let header = h else {
            return UICollectionReusableView()
        }
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: MovieCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as? MovieCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        let movie = self.moviesItens.object(at: indexPath.row) as? Movie
        cell.uiName?.text = movie?.name
        guard let thumb = movie?.thumb else {
            return UICollectionViewCell()
        }
        //cell.backgroundColor = .red
        let url = URL(string:thumb)
        let placeholder = UIImage(named: "poster-placeholder.png")
        cell.uiThumb?.layer.cornerRadius = 2
        cell.uiThumb?.sd_setImage(with: url, placeholderImage: placeholder)
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Cell \(indexPath.row) selected")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let d:Downloader = Downloader(url: "https://sky-exercise.herokuapp.com/api/Movies")
            d.downloadJsonWithURL(completionHandler: { success in
                if(success) {
                self.moviesItens = d.movieArray
                DispatchQueue.main.async {
                self.uiMovieCollection?.reloadData()
                }
                }
        })

        let layout = uiMovieCollection?.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
    
    }

}

