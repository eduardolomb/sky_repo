//
//  DownloadData.swift
//  desafio_sky
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import Foundation

class Downloader {

    var movieArray:NSMutableArray
    var url:String
    
    init(url:String) {
        self.url = url
        self.movieArray = []
    }
    
    func downloadJsonWithURL(completionHandler: @escaping (Bool) -> Void ) {
    
        guard let url = NSURL(string: self.url) as? URL else {
            return
            
        }
        
        URLSession.shared.dataTask(with: url, completionHandler: {(data, response, error) -> Void in
        
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? NSArray {
                
                for movie in json {
                    guard let m = movie as? NSDictionary else {
                        return
                    }
                    self.parse(movie: m)
                    
                }
                completionHandler(true)
            }
        } catch {
            print("Error deserializing JSON: \(error)")
            completionHandler(false)
        }
       
}).resume()
}


    func parse(movie:NSDictionary) {
        let newMovie:Movie = Movie.init(name: movie.object(forKey: "title") as? String, thumb: movie.object(forKey: "cover_url") as? String)
        self.movieArray.add(newMovie)
    }
    
}
