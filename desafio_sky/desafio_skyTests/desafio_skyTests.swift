//
//  desafio_skyTests.swift
//  desafio_skyTests
//
//  Created by Eduardo Lombardi on 02/02/18.
//  Copyright © 2018 Eduardo Lombardi. All rights reserved.
//

import XCTest
@testable import desafio_sky

class desafio_skyTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testAsyncCalback() {
        
        let service = Downloader(url: "https://sky-exercise.herokuapp.com/api/Movies")
        
        let exp = expectation(description: "Downloader downloads JSON from URL above and callbacks are true.")
        
        service.downloadJsonWithURL(completionHandler: { success in
            XCTAssertTrue(success)
            exp.fulfill()
        })
        
        waitForExpectations(timeout: 3) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
}
